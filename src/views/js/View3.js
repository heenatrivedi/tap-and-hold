import React, { Component } from 'react';
import type { Output } from '../Types';

//Import relevant components as required by specs document here
import { Button } from 'aq-miniapp';

/* Import Assets as required by specs document
ex.
import asset from '../../assets/asset.png';
*/

// Import CSS here
import '../css/View3.css';

type Props = {
  output: Output
}

export class View3 extends Component {
  props: Props;

  render() {
    const showSuccess = this.props.status === 'success'
    return (

      <div className='outterCon'>
        <div className="viewContainer justifySpaceAround thirdView">
          <h2>
            {!showSuccess && <span>You failed to reach</span>}
            <span className="count">{this.props.count_target}</span>
          </h2>
          <h3>
            {showSuccess ? 'You did it!' : 'Time\'s Up'}
          </h3>
        </div>
        <div className='footerCon'>
        <Button title="Done" className="btm_btn" onClick={this.props.onClick}/>
        </div>
      </div>
    )
  }
}
