// @flow
import React, { Component } from 'react';

//Import relevant components as required by specs document here
import { Button } from 'aq-miniapp';

/* Import Assets as required by specs document
ex.
import asset from '../../assets/asset.png';
*/

// Import CSS here
import '../css/View.css';

export class View1 extends Component {
  render() {
    return (
    	<div className='outterCon'>
	      <div className="viewContainer justifySpaceAround firstView">
	      	<h2>Help reach
	      		<span>{this.props.count_target}</span>
	      	</h2>
	      	<div className='thumbImg'></div>
	      </div>
	      <div className='footerCon'>
	        <Button title="Start" className="btm_btn" onClick={this.props.onClick}/>
	      </div>
      </div>
    )
  }
}
