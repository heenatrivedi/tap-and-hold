// @flow
import React, { Component } from 'react';
import type { Output } from '../Types';

// Import component to be developed as required by specs document here
// import Comp from '../../components/Comp';

/* Import Assets as required by specs document
ex.
import asset from '../../assets/asset.png';
*/

// Import CSS here
import '../css/View2.css';

/* Define constants here

ex.
const MY_CONSTANT = 42;
*/

export type Props = {
  onClick: () => void,
  onMouseDown: () => void,
  onMouseUp: () => void
};

export class View2 extends Component {

  item: any;

  state: {
    output: Output
  }

  constructor(props: Props){
    super(props);

    this.state = {
      output: {},
      timer:this.getTimer(),
      pressed:false
    }
    this.interval = null
  }
  getTimer(){    
    return Math.floor((Math.random() * 6)+ 1)*10;
  }
  componentDidMount(){
    let timer = this.state.timer;
    if(this.interval){
      clearInterval(this.interval)
    }
    this.interval = setInterval(()=>{
      timer = timer-1
      this.setState({timer:timer})
    },1000)
  }
  componentDidUpdate(prevProps, prevState){
    if(this.state.timer === 0){
      this.props.onClick('failed')
      if(this.state.pressed)
        this.props.onMouseUp()
    }
    if(this.props.user_count >= this.props.count_target){
      if(this.state.pressed)
        this.props.onMouseUp()
      this.props.onClick('success')
    }
  }
  ComponentWillUnmount(){
    clearInterval(this.interval)
  }
  onMouseDown =() =>{
    this.setState({pressed:true})
    this.props.onMouseDown()
  }
  onMouseUp =() =>{
    this.setState({pressed:false})
    this.props.onMouseUp()
  }

  render() {
    return (
      <div className='outterCon'>
        <div className="viewContainer justifySpaceAround secondView">
        <span className="count">{this.props.user_count}</span>
          <h2>
            
            ARE PRESSING AT THE SAME TIME
          </h2>
          <button 
            className='thumbImg' 
            onTouchStart={this.onMouseDown} 
            onTouchEnd={this.onMouseUp} 
            onMouseDown={this.onMouseDown} 
            onMouseUp={this.onMouseUp}
          />
          <h5>TIME LEFT : <span>{this.state.timer < 10? '0' : null}{this.state.timer}</span> Sec</h5>
        </div>
        <div className='footerCon'>
          <span>KEEP PRESSING</span>
        </div>
      </div>
    )
  }
}
