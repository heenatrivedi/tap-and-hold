// @flow
import React, { Component } from 'react';
import {
  StaticCanvas,
  Background,
  CloudStorage,
  defaultLifeCycle
} from 'aq-miniapp';
import {
  View1,
  View2,
  View3,
  fire
} from './js';
import bg from '../assets/background.jpg';

import type { Output } from './Types';

type Props = {
  cloudStorageClient: CloudStorage,
  id?: string,
  data? : Object,
  mode: 'preview' | 'join'
}

export default class View extends Component {
  state: {
    currentPage: number,
    output: Output,
    data: ?Object,
    mode: 'preview' | 'join',
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      currentPage: 1,
      output: {},
      data: props.data,
      mode: props.mode,
      user_count:0,
      count_target : this.getCountTarget(),
      status:''
    }
  }
  getCountTarget(){
    const MAX_COUNT = 200;
    return Math.floor((Math.random() * MAX_COUNT)+ 1);
  }

  componentWillReceiveProps(nextProps: Props) {
    // if(!nextProps.id) {
    //   this.setState({data: nextProps.data, currentPage: 1});
    // }
  }
  componentWillMount(){
    /* Create reference to messages in Firebase Database */
    let messagesRef = fire.database().ref('/user_count');
    messagesRef.on('value', snapshot => {
      this.setState({user_count:snapshot.val()})
    })
  }
  
  _onView1Click() {
    this.setState({currentPage: 2});
  }

  _onView2Click(status) {
    this.setState({currentPage: 3, status});
  }

  _onView3Click() {
    this.setState({currentPage: 1});
    defaultLifeCycle.end();
  }
  increaseUser(){
    let {user_count} =  this.state
    fire.database().ref('user_count').set((user_count+1));
  }
  decreaseUser(){
    let {user_count} =  this.state
    fire.database().ref('user_count').set((user_count-1));
  }

  render() {
    // const data = this.state.data;
    const height = window.innerWidth;
    const width = window.innerHeight;
    let render = <StaticCanvas width={width} height={height}/>
    // if (data) {
      // let source = data.source;
      // if (this.props.additionalInfo && this.props.additionalInfo.passSource) {
      //   source = this.props.additionalInfo.passSource;
      // }
      switch (this.state.currentPage) {
        case 1:
          render = <View1 onClick={this._onView1Click.bind(this)} count_target={this.state.count_target}/>
          break;
        case 2:
          render = <View2 onClick={this._onView2Click.bind(this)} 
            user_count={this.state.user_count} 
            onMouseUp={this.decreaseUser.bind(this)} 
            onMouseDown={this.increaseUser.bind(this)}
            count_target={this.state.count_target}
            />
          break;
        case 3:
          render = <View3 status={this.state.status} onClick={this._onView3Click.bind(this)} count_target={this.state.count_target}/>
          break;
        default:
          break;
      }
    // }
    return (
      <div className='container'>
        <div className='header'>View {this.state.currentPage}</div>
        <Background
          image={bg}
        />
        {render}
      </div>
    );
  }
}
